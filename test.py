import subprocess
import os

inputs = ["", "first word", "second word", "third word"]
exp_out = ["", "first", "second", "third"]
exp_err = ["Строка не найдена", "", "", "", ""]
errors=[]

for i in range(len(inputs)):
    p = subprocess.Popen(["./program"], stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    out, err = p.communicate(input=inputs[i].encode())
    out = out.decode()
    err = err.decode()
    if (out == exp_out[i] and err == exp_err[i]):
        print("_", end="")
    else:
        print(f"Нужный вывод: {exp_out[i]}")
        print(f"Реальный вывод: {out}")
        print(f"Нужная ошибка: {exp_err[i]}")
        print(f"Реальная ошибка: {err}")

    print("\n")
