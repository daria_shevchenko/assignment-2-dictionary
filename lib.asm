global exit
global string_length
global print_string
global print_newline
global print_char
global print_int
global print_uint
global string_equals
global read_char
global read_word
global parse_uint
global parse_int
global string_copy
global print_error

section .text
 
%define SYSCALL_EXIT 60
%define    WRITE   1
%define    STDOUT  1
%define    STDERR  2

; Принимает код возврата и завершает текущий процесс
exit: 
    mov rax, SYSCALL_EXIT
    syscall

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
	mov rax, -1
	.count:
		inc rax
		cmp byte[rdi + rax], 0
		jne .count		
	ret


; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    push rdi
    call string_length
    pop rdi
    mov  rdx, rax; количество символов в rdx
    mov  rsi, rdi; символ в регистр rsi
    mov  rax, WRITE
    mov  rdi, STDOUT
    syscall
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, 0xA
    ; Принимает код символа и выводит его в stdout
    print_char:
        push rdi ; сдвигаем стек на тот символ, который надо печатать
        mov rsi, rsp ; символ в rsi
        pop rdi ; двигаем обратно
        mov rax, WRITE
        mov rdi, STDOUT
        mov rdx, 1
        syscall
        ret

; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    test rdi, rdi ; проверяем знак
    jns print_uint ; если положительное, то выводим
    push rdi    ; значение в стек
    mov rdi, '-' ; минус в регистр
    call print_char ; печатаем минус
    pop rdi
    neg rdi ; меняем знак
    
; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    mov rax, rdi
    mov r11, 10
    push 0
    .loop:
        xor rdx, rdx    
        div r11         ;деление на 10
        add dx, '0'     ;перевод в ASCII
        push rdx        ;в стек остаток
        cmp rax, 0      ;проверяем на ноль
        jne .loop  
    .print_number:
        pop rax         ;проверяем на конец строки
        cmp al, 0       
        je .exit

        mov rdi, rax    
        call print_char        ;вызов вывода символа
        jmp .print_number      ;выводим следующий символ

    .exit:
        ret



; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
     xor rax, rax  ;результат
     xor rdx, rdx  ;длина строки             
    .loop:           
        mov cl, [rsi+rax]
        cmp cl, [rdi+rax]       ;загружаем и сравниваем символы
        jne .noequal            ; если разные, то возвращаем 0
        test cl, cl             
        je .equal           
        inc rax
        jmp .loop   
    .equal:
        mov rax, 1  
        ret
    .noequal:
        xor rax, rax
        ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    mov rax, 0
    mov rdi, 0
    push 0
    mov rdx, 1 ;количество символов для чтения
    mov rsi, rsp ;  rsi устанавливается на вершину стека, указывая на буфер, где будет сохранен введенный символ
    syscall 
    pop rax
    ret
; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
    xor rdx, rdx
    xor rax, rax
    .loop_read_word:
        push rsi
        push rdi
        push rdx
        call read_char
        pop rdx
        pop rdi
        pop rsi

        cmp rax, 0
        je .end_read_word
        ;cmp rax, 0x20
        ;je .space
        cmp rax, 0xa
        je .end_read_word
        ;cmp rax, 0x9
        ;je .space

        mov  byte[rdi+rdx], al
        inc rdx
        cmp rdx, rsi 
        jge .overflow_read_word
        jmp .loop_read_word
    ;.space:
    ;    cmp rdx, 0
    ;    jne .end_read_word
    ;    jmp .loop_read_word
    .overflow_read_word:
        xor rax, rax
        xor rdx, rdx
        ret
    .end_read_word:
        mov byte[rdi+rdx], 0
        mov rax, rdi
        ret
 

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor rax, rax
    xor rcx, 0
    mov r9, 0 
    mov r10, 10 ; используем 10-ную сс
    .loop:
        mov cl, [rdi+r9] ; читаем символ
        cmp cl,  '0'    ; если меньше нуля то выходим
        jl  .end
        cmp cl,  '9'    ; если больше девяти то выходим
        jg  .end
        sub cl,  '0'    ; вычитаем 0 , чтобы получить число
        mul r10         ; умножаем на 10, чтобы учесть новую цифру
        add rax, rcx    ; прибавляем rcx, чтобы учесть предыдущие цифры
        inc r9
        jmp .loop
    .end:
        mov rdx, r9
        ret




; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    xor rax, rax
    mov cl, [rdi]
    cmp rcx, '-'            ;если положительное, то переходим на .positive
    jne parse_uint
    .neg:
        inc rdi             ;убираем знак и обрабатываем как беззнаковое
        call parse_uint 
        test rdx, rdx
        jz .ret
        inc rdx             ;возвращаем обратно
        neg rax             
        jmp .ret
    .ret:
        ret 

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    xor rax, rax
    .loop:
        cmp rax, rdx                    ;если больше чем буфер то выходим
        jge .exit
        mov r8b, byte [rdi+rax]
        mov byte [rsi+rax], r8b         ;запись символа
        cmp byte [rsi+rax], 0           ;проверяем на окончание 
        inc rax                         ;увеличиваем длину 
        jne .loop 
        ret
    .exit:
        xor rax, rax
        ret

print_error:
    push rdi
    call string_length
    pop rsi
    mov rdx, rax
    mov rax, 1
    mov rdi, 2
    syscall
    ret


