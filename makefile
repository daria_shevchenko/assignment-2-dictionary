PYC = python3
ASC = nasm
ASMFLAGS = -felf64

%.o: %.asm	
	@$(ASC) $(ASMFLAGS) -o $@ $<

program: main.o lib.o dict.o
	@ld -o $@ $^

clean: 
	@rm *.o
test:
	@$(PYC) test.py

.PHONY: clean test
