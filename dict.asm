%include "lib.inc"
%define ID 8;
section .text

global find_word

find_word:
    .loop:
        test rsi, rsi
        je .exit
        push rdi            ;rdi указатель на строку, rsi на начало словаря
        push rsi
        add rsi, ID     ;пропускаем id 
        call string_equals
        pop rsi
        pop rdi
        test rax, rax   ;проверяем результат rax. Если строка найдена (результат не равен нулю), текущий указатель на строку rsi записывается в rax
        jnz .exit
        mov rsi, [rsi]  ;следующий элемент -> rsi 
        jmp .loop
    .exit:
        mov rax, rsi
        ret
