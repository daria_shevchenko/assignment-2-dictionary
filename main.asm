%include "dict.inc"
%include "lib.inc"
%include "words.inc"

%define size_buffer 256
%define ID 8

section .data
    buf: times size_buffer db 0

section .rodata
read_err: db "Строка не найдена", 0
find_err: db "Строка превышает размеры", 0

section .text

global _start
_start:
    mov rdi, buf
    mov rsi, size_buffer
    call read_word
    test rax, rax
    jz .find_error

    push rdx

    mov rdi, rax
    mov rsi, PREVIOUS
    call find_word

    pop rdx 

    test rax, rax
    jz .read_error

    mov rdi, rax
    add rdi, ID
    add rdi, rdx
    inc rdi
    call print_string
    jmp .exit

.read_error:
    mov rdi, read_err
    call print_error
    jmp .exit
.find_error:
    mov rdi, find_err
    call print_error
.exit:
    call print_newline
    call exit


